# R package: FOSer 
A collection of R functions for querying DFO's Fisheries Operations System (FOS).

[[_TOC_]]

## Introduction

This package allows for querying the FOS by sending SQL to FOS using the query function (```queryFOS```). The user can take advantage of the included sql (which are the same sql as used for the FOS reports) or obtain, from FOS staff, the SQL behind any FOS report and include that with  (```queryFOS```), or draft their own sql and submit that to the same function. FOS can only be accessed on the DFO network, and this constraint also applies to using the FOSer package.

Once the package is installed, if using Rstudio, you will (eventually) find some basic information in the user guides area of the package documentation. Currently there is a vignette, which has the table relationship diagrams.

## Installation
**NOTE!** Before installing the package you need two additional items installed, and details are found in the readme file on the DFO network folder:\
\\\dcbcpbsna01a.ENT.dfo-mpo.ca\Salmon$\FOS_R

As the readme file indicates in greater detail, in this order:
- From the DFO software center, install Oracle instant client (windows, not R)
- Using the zip file package build made available on the DFO network folder indicated above, install R package: ROracle. Do not install from CRAN.)



To install in R:
    
```{r} 
install.packages("remotes") 
remotes::install_git("https://gitlab.com/michaelfolkes/FOSer", build_vignettes = TRUE) 
```

Load `FOSer` into memory:
```{r} 
require(FOSer)
```

### Installing ROracle with R version 4.2 or higher:
If you try to install ROracle from .zip file with R>= 4.2, you may get an error about UCRT. Below is a work around. 
- Ensure Oracle 12 (Instant Client) x64 is installed from the DFO Software Center. 
- Ensure you have the latest version of [Rtools (4.2)](https://cran.r-project.org/bin/windows/Rtools/rtools42/rtools.html) installed and set up on the path
- Download ROracle 1.3-2 (R-3.6.0) tar.gz file [here](https://www.oracle.com/database/technologies/roracle-downloads.html) (you will need to make a free Oracle account)
- Download the Oracle SDK package [here](https://www.oracle.com/database/technologies/instant-client/winx64-64-downloads.html). Unzip. 
- From the unzipped `sdk` folder, copy and paste the `include` folder into your Oracle Instant Client folder (e.g., the file path would look like this after pasting: `C:\Oracle\12.2.0_Instant_x64\include`)  [mf: this was needed for me: `C:\Oracle\12.2.0_Instant_x64\sdk\include`]
- If Oracle Instant Client folder location is not on your path after installing from the Software Center, run 
```{r}
Sys.setenv(OCI_LIB64= 'C:/Oracle/12.2.0_Instant_x64' )
```
- Install the ROracle package from the source file you downloaded, using the appropriate folder location.
```{r}
install.packages('C:/.../ROracle_1.3-2.tar.gz', repos = NULL, type = "source")
```
- More information can be found  [here](https://stackoverflow.com/questions/73344721/roracle-fails-to-install-as-not-built-for-ucrt)

## Database Connections

The function that creates a connection to FOS needs two files. The first file, `fos.config`, includes the connectivity arguments and can be obtained from either Michael Folkes or Luke Warkenton. The second file is the `fos.userid.config`, which includes the username and password for connecting to FOS. Thus, the user must have an FOS account and be on the DFO network to use this package. To create a template file of `fos.userid.config`, run the function `FOSer::writeUserIDfile()`. Doing this will write a copy to your current working directory. 
**Neither of these files should be shared nor stored on any git platform.**

A third text file, which identifies computer paths that are specific to a user's PC is also helpful to have. This is called the `user.settings.config` file. A template version of this can be created by calling the R function `FOSer::writeUsersettingsfile()`. Doing this will write a copy to your current working directory. You'll then need to open it with your favourite text editor and change the paths to match for your PC.


## Demo scripts!

The function `writeScript` will make it easier to start things. The help file has an example:
```{r} 
?FOSer::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```{r} 
demo(package = "FOSer")
```

To save and open a specific demo script (this one named "demo_aa"):
```{r} 
writeScript("demo_aa")
```

Here is how to open a copy of the script that shows how to query FOS using a few of the Tyee reports:
```{r} 
writeScript("demo_queryFOSreports")
```

## Developing new sql
If you clone this repo to your computer you'll see a folder named `scripts`, or alternatively just download that folder to your computer. In that folder are a few R scripts that include a lot of simple sql for querying mostly tyee related data. Due to the paucity of information on where data reside in FOS, I relied on the diagrams of table relationships (found in the FOSer vignette) and started querying single tables to get sense of their contents and data structure. Learn by exploring...

Giving credit is nice. You can obtain citation details for this R package using the command: 
`citation('FOSer')`.

<!--
## Why the name?
[It could be good, it could be bad.](https://www.urbandictionary.com/define.php?term=foser "urban dictionary's def") Perhaps it's best for the user to decide.
-->
