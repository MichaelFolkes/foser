select species,
         fishing_date,
         multi_avg_catch_per_hour avg_catch_per_hour,
         nvl2 (multi_avg_catch_per_hour,
               sum (multi_avg_catch_per_hour) over (order by fishing_date range unbounded preceding),
               null)
            ttd_catch_per_hour 
                              ,
         round (mono_avg_catch_per_hour * 1016.1) escapement,
         nvl2
         (
            mono_avg_catch_per_hour,
            sum (round (mono_avg_catch_per_hour * 1016.1))
               over (order by fishing_date range unbounded preceding),
            null
         )
            ttd_escapement,
         1016.1 sk_esc_multiplier 
    from (  select ci.tyee_gn_conv_nme species,
                   trunc (cps.fishing_date) fishing_date,
                   round
                   (
                      avg(  cps.total_set_catch
                          * decode (cps.mesh_type, 'MONOFILAMENT', ci.tyee_gn_conv_factor, 1)
                          / cps.total_set_fishing_time),
                      2
                   )
                      multi_avg_catch_per_hour,
                   avg (cps.total_set_catch / cps.total_set_fishing_time)
                      mono_avg_catch_per_hour
              from (  select cr.crpt_id,
                             lic.lic_grp_nme,
                             trunc (cr.crpt_dtt) fishing_date,
                             fe.fe_set_no set_no,
                             (  ( (max (nfo.nfo) - min (nso.nso)) * 0.5)
                              + (min (nsi.nsi) - max (nfo.nfo))
                              + ( (max (nfi.nfi) - min (nsi.nsi)) * 0.5))
                             * 24
                                total_set_fishing_time,
                             sum (nvl (ca.catch_qty, 0)) total_set_catch,
                             upper (regexp_substr (nc.descrip,
                                                   'MONOFILAMENT|MULTIFILAMENT',
                                                   1,
                                                   1,
                                                   'i'))
                                mesh_type
                        from FOS_V1_1.season sn,
                             FOS_V1_1.fishry_opening fo,
                             FOS_V1_1.period pd,
                             FOS_V1_1.crpt_vw cr,
                             FOS_V1_1.fe_vw fe,
                             FOS_V1_1.licence lic,
                             (select ai.atin_id fe_id,
                                     to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS') nso
                                from FOS_V1_1.attr_instance ai
                               where ai.attent_cde = 'FE' and
                                     ai.attr_cde = 'NET_START_OUT' and
                                     ai.atin_val is not null) nso,
                             (select ai.atin_id fe_id,
                                     to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS') nfo
                                from FOS_V1_1.attr_instance ai
                               where ai.attent_cde = 'FE' and
                                     ai.attr_cde = 'NET_FULL_OUT' and
                                     ai.atin_val is not null) nfo,
                             (select ai.atin_id fe_id,
                                     to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS') nsi
                                from FOS_V1_1.attr_instance ai
                               where ai.attent_cde = 'FE' and
                                     ai.attr_cde = 'NET_START_IN' and
                                     ai.atin_val is not null) nsi,
                             (select ai.atin_id fe_id,
                                     to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS') nfi
                                from FOS_V1_1.attr_instance ai
                               where ai.attent_cde = 'FE' and
                                     ai.attr_cde = 'NET_FULL_IN' and
                                     ai.atin_val is not null) nfi,
                             (select *
                                from FOS_V1_1.catch
                               where species_species_cde = '118' and
                                     mat_mat_cde = '2') ca,
                             FOS_V1_1.net_config nc,
                             FOS_V1_1.net_config_dtl ncd,
                             (select ai.atin_id fe_id,
                                     ai.atin_val sc
                                from FOS_V1_1.attr_instance ai
                               where ai.atin_val is not null and
                                     ai.attent_cde = 'FE' and
                                     ai.attr_cde = 'SET_CODE') sc
                       where sn.season_id = fo.season_id and
                             sn.fshry_fishery_id = fo.fshry_fishery_id and
                             fo.opng_id = pd.opng_opng_id and
                             pd.pd_id = cr.pd_pd_id and
                             cr.crpt_id = fe.crpt_crpt_id and
                             lic.lic_id = cr.lic_lic_id and
                             fe.fe_id = nso.fe_id and
                             fe.fe_id = nfo.fe_id and
                             fe.fe_id = nsi.fe_id and
                             fe.fe_id = nfi.fe_id and
                             fe.fe_id = ca.fe_fe_id(+) and
                             nvl (fe.fe_fe_id, fe.fe_id) = sc.fe_id and
                             nc.id = ncd.netcfg_id and
                             fo.opng_id = nc.opng_id and
                             ncd.id = fe.ncd_id and
                             sn.fshry_fishery_id = 8 and
                             fo.fsub_fsub_id = 585 and
                             trunc (cr.crpt_dtt) between trunc(to_date ('06/10/2020 00:00',
                                                                        'mm/dd/yyyy hh24:'||'mi:'||'ss'))
                                                     and  to_date ('10/31/2020 00:00',
                                                                   'mm/dd/yyyy hh24:'||'mi:'||'ss') and
                             cr.cdsrc_cdsrc_id = 1 and
                             sc.sc = '2' and
                             ( (upper (regexp_substr (nc.descrip,
                                                      'MONOFILAMENT|MULTIFILAMENT',
                                                      1,
                                                      1,
                                                      'i')) = 'MULTIFILAMENT' and
                                trunc (fo.opng_end_dtt) <= to_date ('31/12/2001', 'DD/MM/YYYY')) or
                              (upper (regexp_substr (nc.descrip,
                                                     'MONOFILAMENT|MULTIFILAMENT',
                                                     1,
                                                     1,
                                                     'i')) = 'MONOFILAMENT' and
                               fo.opng_start_dtt >= to_date ('01/01/2002', 'DD/MM/YYYY')))
                    group by cr.crpt_id,
                             lic.lic_grp_nme,
                             cr.crpt_dtt,
                             fe.fe_set_no,
                             nc.descrip
                    order by cr.crpt_dtt,
                             fe.fe_set_no) cps,
                   (select tgc.tyee_gn_conv_nme,
                           tgc.dc_species_cde,
                           tgc.dc_mat_cde,
                           tgc.tyee_gn_conv_factor,
                           tgc.tyee_gn_conv_sort
                      from FOS_V1_1.tyee_gn_conv_vw tgc
                     where tgc.dc_species_cde = '118' and
                           tgc.dc_mat_cde = '2') ci
          group by ci.tyee_gn_conv_nme,
                   cps.fishing_date,
                   ci.tyee_gn_conv_factor)
order by fishing_date 