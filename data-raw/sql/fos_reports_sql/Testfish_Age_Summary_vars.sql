select stat_pd.vno, stat_pd.statwk, 
	   decode(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0),0,null,
	   		nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0)) cnt,
	   cnt_3, cnt_4, cnt_5, cnt_6,
	   decode(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0),0,null,
	   		nvl(cnt_3,0)*100/(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0))) pct_3,
	   decode(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0),0,null,
	   		nvl(cnt_4,0)*100/(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0))) pct_4,
	   decode(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0),0,null,
	   		nvl(cnt_5,0)*100/(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0))) pct_5,
	   decode(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0),0,null,
	   		nvl(cnt_6,0)*100/(nvl(cnt_3,0)+nvl(cnt_4,0)+nvl(cnt_5,0)+nvl(cnt_6,0))) pct_6
  from (SELECT ol.opnlic_vessel_no vno, sper_period_cde statwk, count(1) cnt_3
		FROM FOS_V1_1.fishry_opening o,
		     FOS_V1_1.period,
		     FOS_V1_1.crpt_vw,
		     FOS_V1_1.opening_lic ol,
		     FOS_V1_1.stat_period_vw sw,
		     FOS_V1_1.fe_vw,
		     FOS_V1_1.bio_sample bs,
         FOS_V1_1.scale_age_vw sa
		WHERE fsub_fsub_id = '@fsub_id@'
		  and TO_CHAR(opng_start_dtt,'YYYY') = '@year@'
		   and period.opng_opng_id = o.opng_id
		  and crpt_vw.pd_pd_id = period.pd_id
		  and ol.opng_opng_id = o.opng_id
		  and ol.lic_lic_id = crpt_vw.lic_lic_id
		  and sw.sper_yr = to_char(crpt_vw.crpt_dtt,'YYYY')
		  and sw.SPT_SPER_TYPE_CDE = 1
		  and substr(sw.sper_period_cde,4) != '9'
	 
		  and trunc(crpt_dtt) between sw.sper_start_dt and sper_end_dt  -- FE  
     
		  and fe_vw.crpt_crpt_id = crpt_vw.crpt_id
		  and bs.fe_id = fe_vw.fe_id
		  and bs.species_cde = '@speciescode@'
      and sa.scale_age_id = bs.scale_age_id
		  and substr(sa.scale_age_nme,1,1) = '3'
		GROUP BY ol.opnlic_vessel_no, sper_period_cde) age_3,
     (SELECT ol.opnlic_vessel_no vno, sper_period_cde statwk, count(1) cnt_4
		FROM FOS_V1_1.fishry_opening o,
		     FOS_V1_1.period,
		     FOS_V1_1.crpt_vw,
		     FOS_V1_1.opening_lic ol,
		     FOS_V1_1.stat_period_vw sw,
		     FOS_V1_1.fe_vw,
		     FOS_V1_1.bio_sample bs,
         FOS_V1_1.scale_age_vw sa
		WHERE fsub_fsub_id = '@fsub_id@'
		  and TO_CHAR(opng_start_dtt,'YYYY') = '@year@'
		  and period.opng_opng_id = o.opng_id
		  and crpt_vw.pd_pd_id = period.pd_id
		  and ol.opng_opng_id = o.opng_id
		  and ol.lic_lic_id = crpt_vw.lic_lic_id
		  and sw.sper_yr = to_char(crpt_vw.crpt_dtt,'YYYY')
		  and sw.SPT_SPER_TYPE_CDE = 1
		  and substr(sw.sper_period_cde,4) != '9'
	
		  and trunc(crpt_dtt) between sw.sper_start_dt and sper_end_dt   -- FE
      
		  and fe_vw.crpt_crpt_id = crpt_vw.crpt_id
		  and bs.fe_id = fe_vw.fe_id
		  and bs.species_cde = '@speciescode@'
      and sa.scale_age_id = bs.scale_age_id
		  and substr(sa.scale_age_nme,1,1) = '4'
		GROUP BY ol.opnlic_vessel_no, sper_period_cde) age_4,
     (SELECT ol.opnlic_vessel_no vno, sper_period_cde statwk, count(1) cnt_5
		FROM FOS_V1_1.fishry_opening o,
		     FOS_V1_1.period,
		     FOS_V1_1.crpt_vw,
		     FOS_V1_1.opening_lic ol,
		     FOS_V1_1.stat_period_vw sw,
		     FOS_V1_1.fe_vw,
		     FOS_V1_1.bio_sample bs,
         FOS_V1_1.scale_age_vw sa
		WHERE fsub_fsub_id = '@fsub_id@'
		  and TO_CHAR(opng_start_dtt,'YYYY') = '@year@'
		  and period.opng_opng_id = o.opng_id
		  and crpt_vw.pd_pd_id = period.pd_id
		  and ol.opng_opng_id = o.opng_id
		  and ol.lic_lic_id = crpt_vw.lic_lic_id
		  and sw.sper_yr = to_char(crpt_vw.crpt_dtt,'YYYY')
		  and sw.SPT_SPER_TYPE_CDE = 1
		  and substr(sw.sper_period_cde,4) != '9'
	
		  and trunc(crpt_dtt) between sw.sper_start_dt and sper_end_dt  -- FE  
      
		  and fe_vw.crpt_crpt_id = crpt_vw.crpt_id
		  and bs.fe_id = fe_vw.fe_id
		  and bs.species_cde = '@speciescode@'
      and sa.scale_age_id = bs.scale_age_id
		  and substr(sa.scale_age_nme,1,1) = '5'
		GROUP BY ol.opnlic_vessel_no, sper_period_cde) age_5,
     (SELECT ol.opnlic_vessel_no vno, sper_period_cde statwk, count(1) cnt_6
		FROM FOS_V1_1.fishry_opening o,
		     FOS_V1_1.period,
		     FOS_V1_1.crpt_vw,
		     FOS_V1_1.opening_lic ol,
		     FOS_V1_1.stat_period_vw sw,
		     FOS_V1_1.fe_vw,
		     FOS_V1_1.bio_sample bs,
         FOS_V1_1.scale_age_vw sa
		WHERE fsub_fsub_id = '@fsub_id@'
		  and TO_CHAR(opng_start_dtt,'YYYY') = '@year@'
		  and period.opng_opng_id = o.opng_id
		  and crpt_vw.pd_pd_id = period.pd_id
		  and ol.opng_opng_id = o.opng_id
		  and ol.lic_lic_id = crpt_vw.lic_lic_id
		  and sw.sper_yr = to_char(crpt_vw.crpt_dtt,'YYYY')
		  and sw.SPT_SPER_TYPE_CDE = 1
		  and substr(sw.sper_period_cde,4) != '9'
	
          and trunc(crpt_dtt) between sw.sper_start_dt and sper_end_dt  -- FE  
      
		  and fe_vw.crpt_crpt_id = crpt_vw.crpt_id
		  and bs.fe_id = fe_vw.fe_id
		  and bs.species_cde = '@speciescode@'
      and sa.scale_age_id = bs.scale_age_id
		  and substr(sa.scale_age_nme,1,1) = '6'
		GROUP BY ol.opnlic_vessel_no, sper_period_cde) age_6,
	  (select vno, sper_period_cde statwk
		 from FOS_V1_1.stat_period_vw,
		      (select 1 vno from dual
		       union
		       select 2 vno from dual) vn
	    where sper_yr = '@year@'
		  and substr(sper_period_cde,4) != '9'
		  and SPT_SPER_TYPE_CDE = 1 ) stat_pd
		  
   where age_3.vno    (+) = stat_pd.vno
     and age_3.statwk (+) = stat_pd.statwk
     and age_4.vno    (+) = stat_pd.vno
     and age_4.statwk (+) = stat_pd.statwk
     and age_5.vno    (+) = stat_pd.vno
     and age_5.statwk (+) = stat_pd.statwk
     and age_6.vno    (+) = stat_pd.vno
     and age_6.statwk (+) = stat_pd.statwk
		 and (age_3.vno  is not null or age_4.vno  is not null or age_5.vno is not null or age_6.vno is not null)
   order by stat_pd.vno, stat_pd.statwk
