select to_char (trunc (cr.crpt_dtt), 'DD/MON/YYYY') "DATE",
         fe.fe_set_no "SET",
         fe.subset_no panel,
         case
            when fe.subset_no > 1 or
                 ssfe.subset_cnt is not null then
               '="' || msh.mesh_desc || '"'
            else
               'MULTIPANEL'
         end
            mesh_size,
         nso.nso net_start_out,
         nfo.nfo net_full_out,
         nsi.nsi net_start_in,
         nfi.nfi net_full_in,
         fa.fa_nme area,
         sum (decode (ca.species_species_cde, '118', decode (ca.mat_mat_cde, 0, 0, ca.catch_qty), 0))
            sockeye_total,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_sock_lead_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '118',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_sock_lead_reld_bitten,
         sum (decode (ca.species_species_cde, '124', decode (ca.mat_mat_cde, 0, 0, ca.catch_qty), 0))
            chinook_total,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '2',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            lg_chin_lead_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '124',
                decode
                (
                   ca.mat_mat_cde,
                   '3',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            jk_chin_lead_reld_bitten,
         sum (decode (ca.species_species_cde, '108', decode (ca.mat_mat_cde, 0, 0, ca.catch_qty), 0))
            pink_total,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '108',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            pink_lead_reld_bitten,
         sum (decode (ca.species_species_cde, '115', decode (ca.mat_mat_cde, 0, 0, ca.catch_qty), 0))
            coho_total,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '115',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            coho_lead_reld_bitten,
         sum (decode (ca.species_species_cde, '112', decode (ca.mat_mat_cde, 0, 0, ca.catch_qty), 0))
            chum_total,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '112',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            chum_lead_reld_bitten,
         sum (decode (ca.species_species_cde, '128', decode (ca.mat_mat_cde, 0, 0, ca.catch_qty), 0))
            steelhead_total,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '1',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            silver_sthd_lead_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_cork_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_mid_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_lead_kept_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_cork_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_mid_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 0, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_lead_kept_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_cork_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_mid_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_lead_reld_caught,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'CORK', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_cork_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'MIDDLE', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_mid_reld_bitten,
         sum(decode
             (
                ca.species_species_cde,
                '128',
                decode
                (
                   ca.mat_mat_cde,
                   '7',
                   decode (ca.ctchmode_ctchmode_cde,
                           'LEAD', decode (ca.catch_released, 1, ca.bitten_catch_qty, 0),
                           0),
                   0
                ),
                0
             ))
            kelt_sthd_lead_reld_bitten
    from FOS_V1_1.season sn,
         FOS_V1_1.fishry_opening fo,
         FOS_V1_1.period pd,
         FOS_V1_1.opening_lic ol,
         FOS_V1_1.crpt_vw cr,
         FOS_V1_1.fe_vw fe,
         FOS_V1_1.fishry_area fa,
         (select ai.atin_id fe_id,
                 to_char (to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS'), 'DD/MON/YYYY HH24:'||'MI') nso
            from FOS_V1_1.attr_instance ai
           where ai.attent_cde = 'FE' and
                 ai.attr_cde = 'NET_START_OUT') nso,
         (select ai.atin_id fe_id,
                 to_char (to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS'), 'DD/MON/YYYY HH24:'||'MI') nfo
            from FOS_V1_1.attr_instance ai
           where ai.attent_cde = 'FE' and
                 ai.attr_cde = 'NET_FULL_OUT') nfo,
         (select ai.atin_id fe_id,
                 to_char (to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS'), 'DD/MON/YYYY HH24:'||'MI') nsi
            from FOS_V1_1.attr_instance ai
           where ai.attent_cde = 'FE' and
                 ai.attr_cde = 'NET_START_IN') nsi,
         (select ai.atin_id fe_id,
                 to_char (to_date (ai.atin_val, 'MM DD YYYY HH24:'||'MI:'||'SS'), 'DD/MON/YYYY HH24:'||'MI') nfi
            from FOS_V1_1.attr_instance ai
           where ai.attent_cde = 'FE' and
                 ai.attr_cde = 'NET_FULL_IN') nfi,
         (select ca0.catch_qty,
                 nvl (bcq.bcq, 0) bitten_catch_qty,
                 ca0.catch_released,
                 ca0.ctchmode_ctchmode_cde,
                 ca0.fe_fe_id,
                 ca0.mat_mat_cde,
                 ca0.species_species_cde
            from FOS_V1_1.catch ca0,
                 (select ai.atin_id catch_id,
                         to_number (regexp_substr (ai.atin_val, '\d+')) bcq
                    from FOS_V1_1.attr_instance ai
                   where ai.atin_val is not null and
                         ai.attent_cde = 'CATCH' and
                         ai.attr_cde = 'BITTEN_CATCH_QTY') bcq
           where ca0.catch_id = bcq.catch_id(+)) ca,
         FOS_V1_1.net_config_dtl ncd,
         FOS_V1_1.mesh_vw msh,
         (  select fe_fe_id,
                   count (fe_id) + 1 subset_cnt
              from FOS_V1_1.fe_vw
             where fe_fe_id is not null
          group by fe_fe_id) ssfe
   where sn.season_id = fo.season_id and
         sn.fshry_fishery_id = fo.fshry_fishery_id and
         fo.opng_id = pd.opng_opng_id and
         fo.opng_id = ol.opng_opng_id and
         pd.pd_id = cr.pd_pd_id and
         ol.lic_lic_id = cr.lic_lic_id and
         cr.crpt_id = fe.crpt_crpt_id and
         fa.fa_id = fe.fa_fa_id and
         fe.fe_id = nso.fe_id and
         fe.fe_id = nfo.fe_id and
         fe.fe_id = nsi.fe_id and
         fe.fe_id = nfi.fe_id and
         fe.fe_id = ca.fe_fe_id(+) and
         fe.fe_id = ssfe.fe_fe_id(+) and
         fe.ncd_id = ncd.id and
         ncd.mesh_id = msh.mesh_id and
         sn.fshry_fishery_id = 8 and
         fo.fsub_fsub_id = 585 and
         cr.cdsrc_cdsrc_id = 1 and
         trunc (cr.crpt_dtt) between trunc(to_date ('@startdate@','YYYY/MM/DD'))
                                 and to_date ('@enddate@','YYYY/MM/DD')
group by trunc (cr.crpt_dtt),
         fe.fe_set_no,
         fe.subset_no,
         ssfe.subset_cnt,
         msh.mesh_desc,
         nso.nso,
         nfo.nfo,
         nsi.nsi,
         nfi.nfi,
         fa.fa_nme
order by trunc (cr.crpt_dtt),
         fe.fe_set_no,
         fe.subset_no 