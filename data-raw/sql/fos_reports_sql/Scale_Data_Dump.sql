select fo.opng_id, fo.fsub_fsub_id fsub_id, fsub.fsub_nme, fe.fa_fa_id fa_id, fa_nme ||' ('||fa_old_cde1||'-'||fa_old_cde2||')' fa_nme, cr.crpt_id, cr.crpt_dtt, chr(39)||sper_period_cde statwk
, lv.lic_id, nvl(lv.lic_grp_nme,lv.lic_nme) lic_nme, lv.tab lic_tab, ol.opnlic_vessel_no vno, fe.fe_id, fe.fe_set_no,
bio.scale_book_no sbno, bio.scale_no sno, bio.fish_no fno,
nvl(spp.species_common_nme, spp.species_scientific_nme) sppnme,
agevw.scale_age_nme age, sex_vw.sex_nme,
bio.weight wt, unit.dsc_shrt units, 
decode(bio.scale_ind,1,'yes','no') scale_sample, 
scfmt.scale_format_nme scale_format, 
bio.length sclen, lt.length_type_nme lentype, bio.scale_project_no projkey,
bio.scale_form_no formno, 
decode(bio.cwt_ind,1,'yes','no') cwt 
,cwtvw.cwt_detection_nme cwt_detect 
,bio.cwt_tag_code cwt_tag 
,bio.head_tag_no head_tag 
,decode(bio.otolith_ind,1,'yes','no') oto 
,bio.otolith_box_no oto_box 
,bio.otolith_no oto_no 
,decode(bio.genetic_ind,1,'yes','no') genetic_ind 
,bio.genetic_no 
,gen.genetic_type_nme genetic_type 
,mark.mark_nme 
,fcolour.flesh_colour_nme flesh_colour 
,scolour.skin_colour_nme skin_colour 
,physmark.physical_mark_nme phys_mark 
, nvl(ncd.mesh_id, bio.mesh_id) mesh_id
, nvl(mesh.mesh_nme, biomesh.mesh_nme) mesh_nme 
, bio.bio_cmnt bio_cmnt 
from fishry_opening fo, fishery_subtype fsub, period pd, area_vw fa, crpt_vw cr, stat_period_vw sw, 
lic_vw lv, opening_lic ol, fe_vw fe, bio_sample bio, scale_age_vw agevw, length_type_vw lt, sex_vw, species_vw spp, 
unit_vw unit 
,scale_format_vw scfmt 
,cwt_detection_vw cwtvw 
,genetic_type_vw gen 
,mark_vw mark 
,flesh_colour_vw fcolour 
,skin_colour_vw scolour 
,physical_mark_vw physmark 
,net_config_dtl ncd 
,mesh_vw biomesh 
,mesh_vw mesh 
where pd.opng_opng_id  = fo.opng_id 
and fsub.fsub_id	    = fo.fsub_fsub_id 
and fa.fa_id = fe.fa_fa_id 
and cr.pd_pd_id	    = pd.pd_id 
and sw.sper_yr       = to_char(cr.crpt_dtt,'YYYY') 
and sw.SPT_SPER_TYPE_CDE = 1 
and substr(sw.sper_period_cde,4) != '9' 
and to_date(crpt_dtt,'DD/MM/YYYY HH24') between to_date(sw.sper_start_dt,'DD/MM/YYYY HH24') and to_date(sper_end_dt,'DD/MM/YYYY HH24') 
and lv.lic_id		= cr.lic_lic_id 
and ol.opng_opng_id  = fo.opng_id 
and ol.lic_lic_id	= lv.lic_id 
and fe.crpt_crpt_id  = cr.crpt_id 
and bio.FE_id			= fe.fe_id 
and lt.length_type_id	= bio.length_type_id 
and agevw.scale_age_id	= bio.scale_age_id 
and spp.species_cde	= bio.species_cde 
and sex_vw.sex_id		= bio.sex_id 
and unit.cde			= bio.wt_unit_cde 
and scfmt.scale_format_id = bio.scale_format_id 
and cwtvw.cwt_detection_id = bio.cwt_detection_id 
and gen.genetic_type_id = bio.genetic_type_id
 and mark.mark_id = bio.mark_id
 and fcolour.flesh_colour_id = bio.flesh_colour_id
 and scolour.skin_colour_id = bio.skin_colour_id
 and physmark.physical_mark_id = bio.physical_mark_id
 and biomesh.mesh_id (+) = bio.mesh_id
 and ncd.id (+) = fe.ncd_id
 and mesh.mesh_id (+) = ncd.mesh_id 
 and cr.crpt_dtt between to_date('01/01/2022','DD/MM/YYYY') and to_date('30/06/2022','DD/MM/YYYY') -- SET TO THE APPLICABLE DATE
 --and fo.fsub_fsub_id in (565)  -- DERIVED FROM THE FISHERY_SUBTYPE TABLE
 order by crpt_dtt, crpt_id, fe_id, fe_set_no, sbno, fno  