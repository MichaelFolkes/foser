select dt.crpt_dtt, nvl(Mcnt,0) Mcnt, nvl(Fcnt,0) Fcnt,
	   decode(nvl(Mcnt,0)+nvl(Fcnt,0),0,null, nvl(Mcnt,0)+nvl(Fcnt,0)) cnt,
	   decode(nvl(Mcnt,0)+nvl(Fcnt,0),0,null,
	   		nvl(Mcnt,0)*100/(nvl(Mcnt,0)+nvl(Fcnt,0))) Mpct,
	   decode(nvl(Mcnt,0)+nvl(Fcnt,0),0,null,
	   		nvl(Fcnt,0)*100/(nvl(Mcnt,0)+nvl(Fcnt,0))) Fpct
  from (SELECT to_char(crpt_dtt,'YYYY-MM-DD') crpt_dtt, count(1) Mcnt
		  FROM FOS_V1_1.fishry_opening o,
		       FOS_V1_1.period,
		       FOS_V1_1.crpt_vw,
			FOS_V1_1.fe_vw,
		       FOS_V1_1.bio_sample bs,
           FOS_V1_1.sex_vw sex
		  WHERE fsub_fsub_id = '@fsub_id@'
		    and TO_CHAR(opng_start_dtt,'YYYY') = '@year@'
		    and period.opng_opng_id = o.opng_id
		    and crpt_vw.pd_pd_id = period.pd_id
			
		    and fe_vw.crpt_crpt_id = crpt_vw.crpt_id
		    and bs.fe_id = fe_vw.fe_id
		    and bs.species_cde = '@speciescode@'
        and sex.sex_id = bs.sex_id
		    and sex.sex_nme = 'MALE'
		  GROUP BY to_char(crpt_dtt,'YYYY-MM-DD')) M,
  		(SELECT to_char(crpt_dtt,'YYYY-MM-DD') crpt_dtt, count(1) Fcnt
		  FROM FOS_V1_1.fishry_opening o,
		       FOS_V1_1.period,
		       FOS_V1_1.crpt_vw,
			FOS_V1_1.fe_vw,
		       FOS_V1_1.bio_sample bs,
           FOS_V1_1.sex_vw sex
		  WHERE fsub_fsub_id = '@fsub_id@'
		    and TO_CHAR(opng_start_dtt,'YYYY') = '@year@'
		    and period.opng_opng_id = o.opng_id
		    and crpt_vw.pd_pd_id = period.pd_id
			
		    and fe_vw.crpt_crpt_id = crpt_vw.crpt_id
		    and bs.fe_id = fe_vw.fe_id
		    and bs.species_cde = '@speciescode@'
        and sex.sex_id = bs.sex_id
		    and sex.sex_nme = 'FEMALE'
		  GROUP BY to_char(crpt_dtt,'YYYY-MM-DD')) F,
	    (select distinct to_char(crpt_dtt,'YYYYMMDD') crdate,
			    to_char(crpt_dtt,'YYYY-MM-DD') crpt_dtt
		  from FOS_V1_1.fishry_opening o,
		       FOS_V1_1.period,
		       FOS_V1_1.crpt_vw
			 

		  WHERE fsub_fsub_id = '@fsub_id@'
		    and TO_CHAR(opng_start_dtt,'YYYY') = '@year@'
		    and period.opng_opng_id = o.opng_id
		    and crpt_vw.pd_pd_id = period.pd_id
			
			) dt
   where M.crpt_dtt (+) = dt.crpt_dtt
	 and F.crpt_dtt (+) = dt.crpt_dtt
	 and (nvl(Mcnt,0) + nvl(Fcnt,0)) > 0
   order by dt.crdate
