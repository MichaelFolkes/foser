SELECT
*
FROM
FOS_V1_1.fishry_opening fo
LEFT JOIN FOS_V1_1.period pd
on fo.opng_ID= pd.opng_opng_id
where fo.fsub_fsub_id='@fsub_id@' and
trunc (fo.opng_start_dtt) >= trunc(to_date ('@startdate@','YYYY/MM/DD')) and 
trunc (fo.opng_end_dtt) <= trunc(to_date ('@enddate@','YYYY/MM/DD'))
