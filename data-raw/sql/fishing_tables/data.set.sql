SELECT * FROM
(SELECT
crpt.CRPT_ID, fe.FE_ID PARENT_FE_ID, fe.FE_START_DTT FE_START_DATE, fe.FE_SET_NO SET_NO, fe.FE_CMNT FE_COMMENT, fe.FA_FA_ID, fe.GEAR_GEAR_CDE, fe.FE_EFFORT, fe.FE_INSERTDT, fe.FE_MODIFYDT, fe.SUBSET_NO, nc.nme NET_CONFIG, nc.descrip, nc.NET_LENGTH, nc.NET_DEPTH, crpt.cdsrc_cdsrc_id,  ai.ATTR_CDE, ai.atin_val
FROM
((((((FOS_V1_1.fishry_opening fo
LEFT JOIN FOS_V1_1.period pd
on fo.opng_ID= pd.opng_opng_id)
LEFT JOIN FOS_V1_1.crpt_vw crpt
on pd.pd_id = crpt.pd_pd_id)
LEFT JOIN FOS_V1_1.fe_vw fe
on crpt.crpt_id= fe.crpt_crpt_id)
LEFT JOIN FOS_V1_1.net_config_dtl ncd
on fe.ncd_id = ncd.id)
LEFT JOIN FOS_V1_1.net_config nc
on ncd.netcfg_id= nc.id)
LEFT JOIN FOS_V1_1.attr_instance ai
ON fe.fe_id= ai.ATIN_ID)
where ai.attent_cde = 'FE' and
fo.fsub_fsub_id='@fsub_id@' and 
trunc (fe.FE_START_DTT) >= trunc(to_date ('@startdate@','YYYY/MM/DD')) and 
trunc (fe.FE_START_DTT) <= trunc(to_date ('@enddate@','YYYY/MM/DD')) and
fe.subset_no=1 and
ai.ATTR_CDE in ('SET_CODE', 'RAINCODE', 'GAUGE_HGT', 'FETYPE', 'FE_WATER_TEMP', 'SURFACE_COND', 'SHORE_PANEL', 'TC_WIND_SPEED', 'TC_DEBRIS_COND', 'TC_SEALS_WKNG_NET', 'TC_SKY', 'TC_TURBIDITY',  'TIDE', 'TDE_STG_START_TM', 'WIND_DIRECTION', 'TC_NET_NUMBER', 'START_LAT_DDMM', 'START_LONG_DDMM', 'LOG_SHEET_RECORDER', 'CLOUD', 'FOG')
)
PIVOT
(
 MIN(atin_val)
 FOR ATTR_CDE IN ('SET_CODE' SET_CODE, 'RAINCODE' RAINCODE, 'GAUGE_HGT' GAUGE_HGT, 'FETYPE' FETYPE, 'FE_WATER_TEMP' FE_WATER_TEMP, 'SURFACE_COND' SURFACE_COND, 'SHORE_PANEL' SHORE_PANEL, 'TC_WIND_SPEED' TC_WIND_SPEED, 'TC_DEBRIS_COND' TC_DEBRIS_COND, 'TC_SEALS_WKNG_NET' TC_SEALS_WKNG_NET, 'TC_SKY' TC_SKY, 'TC_TURBIDITY' TC_TURBIDITY, 'TIDE' TIDE, 'TDE_STG_START_TM' TDE_STG_START_TM, 'WIND_DIRECTION' WIND_DIRECTION, 'TC_NET_NUMBER' TC_NET_NUMBER, 'START_LAT_DDMM' START_LAT_DDMM, 'START_LONG_DDMM' START_LONG_DDMM, 'LOG_SHEET_RECORDER' LOG_SHEET_RECORDER, 'CLOUD' CLOUD, 'FOG' FOG))