SELECT
crpt.crpt_id, fe.fe_id, fe.FE_START_DTT FE_START_DATE, fe.FE_SET_NO SET_NO, fe.subset_no, ca.*, ca.species_species_cde species_cde, mat.mat_desc, mat.mat_nme, mat.mat_nme maturity
FROM
(((((FOS_V1_1.fishry_opening fo
LEFT JOIN FOS_V1_1.period pd
on fo.opng_ID= pd.opng_opng_id)
LEFT JOIN FOS_V1_1.crpt_vw crpt
on pd.pd_id = crpt.pd_pd_id)
LEFT JOIN FOS_V1_1.fe_vw fe
on crpt.crpt_id= fe.crpt_crpt_id)
LEFT JOIN FOS_V1_1.CATCH ca
on fe.fe_id = ca.fe_fe_id)
LEFT JOIN FOS_V1_1.maturity mat
on ca.mat_mat_cde = mat.mat_cde)
where fo.fsub_fsub_id='@fsub_id@' and 
trunc (fe.FE_START_DTT) >= trunc(to_date ('@startdate@','YYYY/MM/DD')) and 
trunc (fe.FE_START_DTT) <= trunc(to_date ('@enddate@','YYYY/MM/DD')) and 
ca.species_species_cde in ('108', '112', '115', '118', '124', '128')
