SELECT * FROM
(
SELECT
crpt.CRPT_ID, crpt.crpt_dtt, fe.FE_ID SUBSET_FE_ID, fe.FE_START_DTT FE_START_DATE,fe.FE_SET_NO SET_NO, fe.FE_CMNT, fe.FA_FA_ID, fe.GEAR_GEAR_CDE, fe.CRPT_CRPT_ID, fe.FE_EFFORT, fe.FE_INSERTDT, fe.FE_MODIFYDT, fe.SUBSET_NO, crpt.cdsrc_cdsrc_id,  msh.*, ai.attr_cde, ai.atin_val
FROM
((((((FOS_V1_1.fishry_opening fo
LEFT JOIN FOS_V1_1.period pd
on fo.opng_ID= pd.opng_opng_id)
LEFT JOIN FOS_V1_1.crpt_vw crpt
on pd.pd_id = crpt.pd_pd_id)
LEFT JOIN FOS_V1_1.fe_vw fe
on crpt.crpt_id= fe.crpt_crpt_id)
LEFT JOIN FOS_V1_1.net_config_dtl ncd
on fe.ncd_id = ncd.id)
LEFT JOIN FOS_V1_1.mesh_vw msh
on ncd.mesh_id = msh.mesh_id)
LEFT JOIN FOS_V1_1.attr_instance ai
ON fe.fe_id= ai.ATIN_ID)
where fo.fsub_fsub_id='@fsub_id@' and
trunc (fe.FE_START_DTT) >= trunc(to_date ('@startdate@','YYYY/MM/DD')) and 
trunc (fe.FE_START_DTT) <= trunc(to_date ('@enddate@','YYYY/MM/DD')) and
ai.ATTR_CDE in ('NET_START_OUT', 'NET_FULL_OUT', 'NET_START_IN','NET_FULL_IN')
)
PIVOT(
 MIN(atin_val)
 FOR ATTR_CDE IN ('NET_START_OUT' NET_START_OUT, 'NET_FULL_OUT' NET_FULL_OUT, 'NET_START_IN' NET_START_IN,'NET_FULL_IN' NET_FULL_IN))
ORDER BY FE_START_DATE
