#' Show a text object for re-saving
#'
#' @param x
#'
#' @return
#' @export
#'
#' @examples
#' \dontrun{
#' showTextobj(c("abc","123"))
#' }
showTextobj <- function(x){
	Tfile <- tempfile(fileext = ".txt")
	writeLines(gsub("\r", "",x), con = Tfile)

	file.show(file = Tfile)
}#END showTextobj




#' Read YAML file and load paths to environment
#'
#' @param x character. The path to a yaml file having the user specific paths for other files
#'
#' @return
#' @export
#'
#' @examples
importUserpathsfile <- function(x="userpaths.config"){
	list2env(yaml::read_yaml(x), envir = .GlobalEnv)
}#END importUserpathsfile




#' Write template of userid text file (.config)
#'
#' @param path Character. Path to write file to. Default is current directory
#' @param filename Character. Default is 'fos.userid.config'
#'
#' @details Once this userid file is written, it should be edited so that the user's personal id and password are included. This file should not be shared nor stored on any git platform.
#'
#' @return
#' @export
#'
#' @examples
#' \dontrun{
#' writeUserIDfile()
#' file.edit("fos.userid.config")
#' }
writeUserIDfile <- function(path=".", filename="fos.userid.config"){
	x <- "username: myfosuserid\npassword: myfospassword"
	filepath <- paste(path, filename, sep = "/")
	if(file.exists(filepath)) stop(paste("\nThe file:\n", filepath, "\nalready exists. Rename it or move it before using this function."))
	write(x, file = filepath)

}#END writeUserIDfile





#' Write template of user settings text file (.config)
#'
#' @param path Character. Path to write file to. Default is current directory
#' @param filename Character. Default is 'user.settings.config'
#'
#' @details This is a yaml text file that defines the path variables to at least two other .config files: 'fos.config' and 'fos.userid.config'. Additionally, this is a place to identify user specific paths that one wants to load for use in a shared R script. These can include paths to import or export data. This file should not be shared nor stored on any git platform. The FOSer project .gitignore includes .config files (i.e. .config files will not get pushed and pulled to git platform).
#'
#' @return
#' @export
#'
#' @examples
#' \dontrun{
#' writeUsersettingsfile()
#' file.edit("user.settings.config")
#' }
writeUsersettingsfile <- function(path=".", filename="user.settings.config"){
	x <- '#identify your paths to each of these files
config.filepath: "fos.config"
userid.filepath: "fos.userid.config"

#this is where the exported rds file will go.  will need to revise path to suite your drive:
#if left as ".", this means the current working directory
path.export: "."
'

	filepath <- paste(path, filename, sep = "/")
	if(file.exists(filepath)) stop(paste("\nThe file:\n", filepath, "\nalready exists. Rename it or move it before using this function."))
	write(x, file = filepath)

}#END writeUsersettingsfile
