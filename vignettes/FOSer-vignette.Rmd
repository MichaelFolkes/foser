---
title: "FOSer-vignette"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{FOSer-vignette}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(FOSer)
```

## first section.

hello world.

![FOS page1](FOS ERD 2017-11_Page1.png){width=100%}
![FOS page2](FOS ERD 2017-11_Page2.png){width=100%}
![FOS page3](FOS ERD 2017-11_Page3.png){width=100%}
![FOS page4](FOS ERD 2017-11_Page4.png){width=100%}
![FOS page5](FOS ERD 2017-11_Page5.png){width=100%}
![FOS page6](FOS ERD 2017-11_Page6.png){width=100%}
![FOS page7](FOS ERD 2017-11_Page7.png){width=100%}
![FOS page8](FOS ERD 2017-11_Page8.png){width=100%}
